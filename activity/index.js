function Pokemon(name, lvl, hp){

    this.name = name;
    this.lvl = lvl;
    this.health = hp * 2;
    this.attack = lvl;

    this.tackle = function(target){
        console.log(`${this.name} tackled ${target.name}`);
        target.health = target.health - this.attack;
        console.log(`${target.name} health is now reduced ${target.health}`);
	if(target.health <= 0) {
		console.log(`${target.name} has fainted`);
	}
    };

};

let pidgeot = new Pokemon("Pidgeot", 10, 60);
let raichu = new Pokemon("Raichu", 5, 15);

pidgeot.tackle(raichu);
raichu.tackle(pidgeot);
pidgeot.tackle(raichu);
raichu.tackle(pidgeot);
pidgeot.tackle(raichu);